<?php
namespace App\Controller;

use App\Entity\Student;
use App\Form\StudentType;
use App\Repository\StudentRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
	private $em;

	public function __construct(
		EntityManagerInterface $em
	){
		$this->em = $em;
	}

	#[Route('/', name: 'app_index')]
	public function index(StudentRepository $studentRepository)
	{
		$students = $studentRepository->findAll();

		return $this->render('index.html.twig', [
			'students' => $students
		]);
	}

	#[Route('/add', name: "app_student_add")]
	public function add(Request $request)
	{
		$student = new Student();
		$form = $this->createForm(StudentType::class, $student);

		$form->handleRequest($request);

		if($form->isSubmitted() && $form->isValid()){
			$this->em->persist($student);
			$this->em->flush();

			return $this->redirectToRoute('app_index');
		}

		return $this->render('add.html.twig',[
			'form' => $form->createView()
		]);
	}

	#[Route('/edit/{id}', name: "app_student_edit")]
	public function edit(Student $student, Request $request)
	{
		$form = $this->createForm(StudentType::class, $student);

		$form->handleRequest($request);

		if($form->isSubmitted() && $form->isValid()){
			$this->em->flush();

			return $this->redirectToRoute('app_index');
		}

		return $this->render('edit.html.twig',[
			'form' => $form->createView(),
			'student' => $student
		]);
	}

	#[Route('/delete/{id}', name: "app_student_delete")]
	public function delete(Student $student)
	{
		$this->em->remove($student);
		$this->em->flush();

		return $this->redirectToRoute('app_index');
	}
}
